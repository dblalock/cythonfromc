#!/bin/bash

CYTHON_DIR=src_py
C_DIR=src_c
GEN_DIR=gen

# ensure that the directory in which we place the generated c files exists
if [ ! -d "$GEN_DIR" ]; then
	mkdir -p $GEN_DIR;
fi

# create c files from our Cython code
python $CYTHON_DIR/setup.py build_ext
cp $CYTHON_DIR/*.c $GEN_DIR/
cp $CYTHON_DIR/*.h $GEN_DIR/		# doesn't seem to be necessary; not sure why...
rm $CYTHON_DIR/*.so &> /dev/null	# suppress output

# copy normal c source into same directory as generated c source so
# we can compile them all together easily
cp $C_DIR/* $GEN_DIR/

# print out all the c files we're compliling and how big
# they are (I'm curious about what Cython's generating)
# for f in `ls *.c`
# do
# 	lines=`cat $f | wc -l`
# 	echo -e "compiling file $f: $lines lines"
# done;

# build everything and run it
make clean && make && ./cythonExample.out
