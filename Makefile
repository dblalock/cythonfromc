
# Compiler
CC = gcc
OPTS = -O2 -Wall -Werror

# Project name
PROJECT = cythonExample.out

# Directories
OBJDIR = obj
SRCDIR = gen
CYTHON_OUT_DIR = build	# cython creates this, but we don't need it

# Libraries
LIBS = -lpython2.6

# Files and folders
SRCS    = $(shell find $(SRCDIR) -name '*.c')
SRCDIRS = $(shell find . -name '*.c' | dirname {} | sort | uniq | sed 's/\/$(SRCDIR)//g' )
# SRCDIRS = $(shell find $(SRCDIR) -type d | sed 's/$(SRCDIR)/./g' )	# possibly better version?
OBJS    = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCS))

# Targets
$(PROJECT): buildrepo $(OBJS)
	$(CC) $(OBJS) $(LIBS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(OPTS) -c $< -o $@
	
clean:
	rm -rf $(PROJECT) $(OBJDIR)
	rm -rf $(CYTHON_OUT_DIR)
	
buildrepo:
	@$(call make-repo)

# Create obj directory structure
define make-repo
	mkdir -p $(OBJDIR)
	for dir in $(SRCDIRS); \
	do \
		mkdir -p $(OBJDIR)/$$dir; \
	done
endef