
import numpy as np

cdef extern from "math.h":
	double log(double x)
	int floor(double x)

cdef public double cython_func( double value, double value2 ):
	return value + value2
	 
cdef public int doStuff(int * ar, int len):
	pyVal = doNumpyStuff(len)
	cVal = doCStuff(len)
	return pyVal + cVal;
	
def doNumpyStuff(int len):
	a = np.zeros((1,len)) + len;
	cdef int sum = 0
	for i from 0 <= i < len:
		sum = sum + a[0,i]
	cdef int weirdNum = floor(log(sum))
	return weirdNum

cdef int doCStuff(len):
	cdef int x = 4
	for i from 0 <= i < len:
		x = x+1
	return x