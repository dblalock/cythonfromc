from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize

CYTHON_DIR = 'src_py/'

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = cythonize(CYTHON_DIR + '*.pyx*')
)