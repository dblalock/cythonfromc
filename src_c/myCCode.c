#ifdef __cplusplus {
extern "C"
#endif

double cython_func( double value, double value2 );

#ifdef __cplusplus {
}
#endif

#include <Python.h>
#include "cyFunc.h"

int main() {
	Py_Initialize();
	initcyFunc();
	
	double x = 4;
	double y = 5;
	double res = cython_func(x,y);
	printf("result = %.1lf\n", res);
	int ar [] = {1,2,3};
	int res2 = doStuff(ar, 3);
	printf("result2 = %d\n", res2);
	
	Py_Finalize();
	return 0;
}
