
## Cython From C Example

I recenty started using Cython, and had a little bit of trouble finding examples of how to call Cython-generated code from C. This is a library I put together both to figure it out for my own use, and as an example for anyone else trying to do the same thing.

The basic way it works is this:

- There are two source directories:
	1. _src_py_, which contains all the Cython code and the python script to generate the c files.
	2. *src_c*, which contains the C code that uses these files
- There's a bash script that generates the C from Cython, combines the C in both directories into one directory, calls make to build everything, and then executes the final binary.

Hope this helps you.